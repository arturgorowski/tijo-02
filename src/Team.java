public class Team {

    private int id;
    private String name;
    private int points;
    private int wins;
    private int loses;
    private int draws;

    Team(int id, String name, int points, int wins, int loses, int draws) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
    }

    int getId() {
        return id;
    }

    String getName() {
        return name;
    }

    int getPoints() {
        return points;
    }

    int getWins() {
        return wins;
    }

    int getLoses() {
        return loses;
    }

    int getDraws() {
        return draws;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", points=" + points +
                ", wins=" + wins +
                ", loses=" + loses +
                ", draws=" + draws +
                '}';
    }
}