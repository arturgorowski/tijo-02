import java.util.List;

public interface CsvReport {
    /** funkcja zwraca liste stringow - lista linii z pliku CSV */
    List<String> csvToListOfString();
}