import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvReader implements CsvReport {

    private String fileName;

    CsvReader(String fileName) {
        this.fileName = fileName;

    }

    @Override
    public List<String> csvToListOfString() {

        String line;
        List<String> listOfStrings = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            br.readLine();
            while ((line = br.readLine()) != null) {

                String[] lines = line.split("\n");

                listOfStrings.addAll(Arrays.asList(lines));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfStrings;
    }
}