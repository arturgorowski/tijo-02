import java.util.ArrayList;
import java.util.List;

public class LineConverterImpl implements LineConverter {
    @Override
    public List<Team> toTeam(List<String> lines, String splitBy) {

        List<Team> teams = new ArrayList<>();
        String[] arrSplited;

        for(String line: lines){
            arrSplited = line.split(splitBy);
            Team team = new Team(
                    Integer.parseInt(arrSplited[0].trim()),
                    arrSplited[1].trim(),
                    Integer.parseInt(arrSplited[3].trim()),
                    Integer.parseInt(arrSplited[4].trim()),
                    Integer.parseInt(arrSplited[6].trim()),
                    Integer.parseInt(arrSplited[5].trim())

            );
            teams.add(team);
        }

        return teams;
    }
}