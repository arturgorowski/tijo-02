import java.util.List;

public class Main {
    public static void main(String[] args) {
        final String CSV_FILE = "/home/artur/Pobrane/ekstraklasa.csv";
        final String SPLIT_BY = ";";

        CsvReport csvReport = new CsvReader(CSV_FILE);
        LineConverter lineConverter = new LineConverterImpl();
        List<String> lines = csvReport.csvToListOfString();
        List<Team> teams = lineConverter.toTeam(lines, SPLIT_BY);

        for(Team team: teams) {
            System.out.printf("%2d: %-25s: Punkty: %2d, Zwyciestwa: %2d, Porazki: %2d, Remisy: %2d \n",
                    team.getId(), team.getName(), team.getPoints(), team.getWins(), team.getLoses(),
                    team.getDraws());
        }
    }
}